import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import TabNavigator from './src/navigation/TabNavigator';
import UserSchema from './src/models/UserSchema';

let realm;

const App = () => {
    realm = new Realm({ path: "RNRealm.realm", schema: [UserSchema] });

    return (
        <NavigationContainer>
            <TabNavigator />
        </NavigationContainer>
    );
};

export default App;
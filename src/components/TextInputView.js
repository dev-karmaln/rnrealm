import React from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
} from 'react-native';
import Colors from '../constants/Colors';

const TextInputView = (props) => {
    return (
        <View style={styles.inputContainer}>
            <Text style={styles.inputTitle}>{props.title}</Text>
            <TextInput
                style={styles.textInput}
                value={props.value}
                placeholder={props.placeholder}
                multiline={props.multiline}
                numberOfLines={props.numberOfLines}
                onChangeText={props.onChangeText} />
            {
                props.error && (
                    <Text style={styles.errorText}>{props.errorText}</Text>
                )
            }
        </View>
    );
}

const styles = StyleSheet.create({
    inputContainer: {
        marginVertical: 10,
        marginHorizontal: 16
    },

    inputTitle: {
        color: Colors.colorBlack,
        fontWeight: 'bold',
        fontSize: 16
    },

    textInput: {
        borderWidth: 1,
        borderColor: Colors.colorPrimary,
        marginTop: 8
    },

    errorText: {
        color: Colors.colorRed,
        fontWeight: 'bold',
        fontSize: 14,
        textAlign: 'center'
    }
});

export default TextInputView;
import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

const ListItem = (props) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={props.onPress}>
            <View style={styles.row}>
                <Image source={{ uri: props.avtarUrl }} style={styles.pic} />
                <View>
                    <View style={styles.nameContainer}>
                        <Text style={styles.nameTxt}>{props.title}</Text>
                    </View>
                    <View style={styles.end}>
                        <Text style={styles.description}>{props.description}</Text>
                    </View>
                </View>
                <TouchableOpacity onPress={props.onDelete}>
                    <Image
                        style={[styles.icon, { marginRight: 50 }]}
                        source={{ uri: "https://img.icons8.com/color/48/000000/delete-forever.png" }} />
                </TouchableOpacity>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    row: {
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: '#dcdcdc',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        padding: 10,
        justifyContent: 'space-between',

    },

    pic: {
        borderRadius: 25,
        width: 50,
        height: 50,
    },

    nameContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 270,
    },

    nameTxt: {
        marginLeft: 15,
        fontWeight: '600',
        color: '#222',
        fontSize: 15,

    },

    mblTxt: {
        fontWeight: '200',
        color: '#777',
        fontSize: 13,
    },

    end: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    description: {
        fontWeight: '400',
        color: '#666',
        fontSize: 12,
        marginLeft: 15,
        width: "70%"
    },

    icon: {
        height: 35,
        width: 30,
    }
});

export default ListItem;
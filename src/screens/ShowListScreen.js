import React, { useCallback, useEffect, useState } from 'react';
import {
    Alert,
    FlatList,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import ListItem from '../components/ListItem';
import Realm from 'realm';
import { useFocusEffect } from '@react-navigation/native';

let realm = new Realm({ path: "RNRealm.realm" });

const ShowListScreen = ({ navigation, route }) => {

    const [users, setUsers] = useState([]);

    useEffect(() => {
        realm.write(() => {
            setUsers(realm.objects("User"));
        });
    }, [navigation]);

    useFocusEffect(
        useCallback(() => {
            setUsers(realm.objects("User"));
        }, [])
    );

    const handleUpdateItem = (item) => {
        navigation.navigate("EditScreen", {
            userId: item.id,
            title: item.title
        });
    }

    const handleDeleteItem = (item) => {
        Alert.alert(
            "Are you Sure you wants to delete",
            "Title : " + item.title,
            [
                { text: "OK", onPress: () => deleteItem(item.id) },
                { text: "cancel" }
            ]
        );
    }

    const deleteItem = (id) => {
        realm.write(() => {
            if (realm.objects('User').filtered('id =' + id).length > 0) {
                realm.delete(realm.objects('User').filtered('id =' + id))
            }

            setUsers(realm.objects("User"));
        });
    }

    if (users.length <= 0) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                <Text style={{ alignItems: 'center', }}>No Data found. Try To insert Data.</Text>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <FlatList
                data={users}
                keyExtractor={item => item.id}
                renderItem={(itemData) => (
                    <ListItem
                        avtarUrl={itemData.item.avtarUrl}
                        title={itemData.item.title}
                        description={itemData.item.description}
                        onPress={() => handleUpdateItem(itemData.item)}
                        onDelete={() => handleDeleteItem(itemData.item)} />
                )}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

export default ShowListScreen;
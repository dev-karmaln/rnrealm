import React, { useCallback, useEffect, useState } from 'react';
import {
    Alert,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import Colors from '../constants/Colors';
import TextInputView from "../components/TextInputView";
import Realm from 'realm';

const CreateScreen = ({ navigation, route }) => {

    const [title, setTitle] = useState("");
    const [titleError, setTitleError] = useState(false);
    const [titleErrorText, setTitleErrorText] = useState("");

    const [avtarUrl, setAvtarUrl] = useState("");
    const [avtarUrlError, setAvtarUrlError] = useState(false);
    const [avtarUrlErrorText, setAvtarUrlErrorText] = useState("");

    const [description, setDescription] = useState("");
    const [descriptionError, setDescriptionError] = useState(false);
    const [descriptionErrorText, setDescriptionErrorText] = useState("");
    let realm = new Realm({ path: "RNRealm.realm" });

    const handleAddData = () => {
        setTitleError(false);
        setAvtarUrlError(false);
        setDescriptionError(false);

        if (title.toString().trim().length <= 0) {
            setTitleError(true);
            setTitleErrorText("Empty Title !!");
        } else if (avtarUrl.toString().trim().length <= 0) {
            setAvtarUrlError(true);
            setAvtarUrlErrorText("Empty Avtar URL !!");
        } else if (description.toString().trim().length <= 0) {
            setDescriptionError(true);
            setDescriptionErrorText("Empty Description !!");
        } else {

            const ids = new Date().getTime();
            // TODO: Add Data to Database

            realm.write(() => {
                realm.create("User", {
                    id: ids,
                    title: title,
                    description: description,
                    avtarUrl: avtarUrl
                });

                Alert.alert(
                    "Record Added successfully",
                    "ID : " + ids
                );
            });

            navigation.navigate("HomeNavigator");

            setTitle("");
            setAvtarUrl("");
            setDescription("");
        }
    }

    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.container}>

                    <TextInputView
                        title="Enter Title"
                        placeholder="Title"
                        value={title}
                        numberOfLines={1}
                        multiline={false}
                        onChangeText={value => setTitle(value)}
                        error={titleError}
                        errorText={titleErrorText} />

                    <TextInputView
                        title="Enter Avtar URL"
                        placeholder="Avtar URL"
                        value={avtarUrl}
                        numberOfLines={1}
                        multiline={false}
                        onChangeText={value => setAvtarUrl(value)}
                        error={avtarUrlError}
                        errorText={avtarUrlErrorText} />

                    <TextInputView
                        title="Enter Description"
                        placeholder="Description"
                        value={description}
                        numberOfLines={4}
                        multiline={true}
                        onChangeText={value => setDescription(value)}
                        error={descriptionError}
                        errorText={descriptionErrorText} />

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={styles.button}
                        onPress={handleAddData}>
                        <Text style={styles.buttonText}>Submit</Text>
                    </TouchableOpacity>

                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    button: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.colorPrimary,
        marginHorizontal: 24,
        marginVertical: 16,
        padding: 10,
        borderRadius: 24
    },

    buttonText: {
        fontSize: 18,
        color: Colors.colorWhite
    }
});

export default CreateScreen;
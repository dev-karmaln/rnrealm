import React, { useState } from 'react';
import {
    Alert,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

import Realm from 'realm';
import TextInputView from '../components/TextInputView';

const realm = new Realm({ path: "RNRealm.realm" });

const EditScreen = ({ navigation, route }) => {

    const { userId } = route.params;
    const user = realm.objects('User').filtered('id =' + userId);

    const [title, setTitle] = useState(user[0].title);
    const [titleError, setTitleError] = useState(false);
    const [titleErrorText, setTitleErrorText] = useState("");

    const [avtarUrl, setAvtarUrl] = useState(user[0].avtarUrl);
    const [avtarUrlError, setAvtarUrlError] = useState(false);
    const [avtarUrlErrorText, setAvtarUrlErrorText] = useState("");

    const [description, setDescription] = useState(user[0].description);
    const [descriptionError, setDescriptionError] = useState(false);
    const [descriptionErrorText, setDescriptionErrorText] = useState("");

    const handleEditData = () => {
        setTitleError(false);
        setAvtarUrlError(false);
        setDescriptionError(false);

        if (title.toString().trim().length <= 0) {
            setTitleError(true);
            setTitleErrorText("Empty Title !!");
        } else if (avtarUrl.toString().trim().length <= 0) {
            setAvtarUrlError(true);
            setAvtarUrlErrorText("Empty Avtar URL !!");
        } else if (description.toString().trim().length <= 0) {
            setDescriptionError(true);
            setDescriptionErrorText("Empty Description !!");
        } else {

            realm.write(() => {
                user[0].title = title;
                user[0].avtarUrl = avtarUrl;
                user[0].description = description;

                Alert.alert(
                    "Record Updated successfully"
                );
            });

            navigation.goBack();
        }
    }

    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.container}>

                    <TextInputView
                        title="Enter Title"
                        placeholder="Title"
                        value={title}
                        numberOfLines={1}
                        multiline={false}
                        onChangeText={value => setTitle(value)}
                        error={titleError}
                        errorText={titleErrorText} />

                    <TextInputView
                        title="Enter Avtar URL"
                        placeholder="Avtar URL"
                        value={avtarUrl}
                        numberOfLines={1}
                        multiline={false}
                        onChangeText={value => setAvtarUrl(value)}
                        error={avtarUrlError}
                        errorText={avtarUrlErrorText} />

                    <TextInputView
                        title="Enter Description"
                        placeholder="Description"
                        value={description}
                        numberOfLines={4}
                        multiline={true}
                        onChangeText={value => setDescription(value)}
                        error={descriptionError}
                        errorText={descriptionErrorText} />

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={styles.button}
                        onPress={handleEditData}>
                        <Text style={styles.buttonText}>Update</Text>
                    </TouchableOpacity>

                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    button: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.colorPrimary,
        marginHorizontal: 24,
        marginVertical: 16,
        padding: 10,
        borderRadius: 24
    },

    buttonText: {
        fontSize: 18,
        color: Colors.colorWhite
    }
});

export default EditScreen;
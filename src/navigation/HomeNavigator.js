import React from 'react';
import { Platform } from "react-native";
import { createStackNavigator } from '@react-navigation/stack';

import ShowListScreen from '../screens/ShowListScreen';
import EditScreen from '../screens/EditScreen';

import Colors from "../constants/Colors";

const Stack = createStackNavigator();

const HomeNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="ShowListScreen"
                component={ShowListScreen}
                options={{
                    title: 'All Data',
                    headerStyle: {
                        backgroundColor: Platform.OS === "android" ? Colors.colorPrimary : Colors.colorWhite,
                    },
                    headerTintColor: Platform.OS === "android" ? Colors.colorWhite : Colors.colorPrimary,
                }} />

            <Stack.Screen
                name="EditScreen"
                component={EditScreen}
                options={
                    ({ route }) => ({
                        title: route.params.title,
                        headerStyle: {
                            backgroundColor: Platform.OS === "android" ? Colors.colorPrimary : Colors.colorWhite,
                        },
                        headerTintColor: Platform.OS === "android" ? Colors.colorWhite : Colors.colorPrimary,
                    })
                } />
        </Stack.Navigator>
    );
};

export default HomeNavigator;
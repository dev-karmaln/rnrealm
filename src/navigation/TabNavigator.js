import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeNavigator from './HomeNavigator';
import CreateNavigator from './CreateNavigator';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
    return (
        <Tab.Navigator
            initialRouteName="HomeNavigator"
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'HomeNavigator') {
                        iconName = focused ? 'ios-home' : 'ios-home-outline';
                    } else if (route.name === 'CreateNavigator') {
                        iconName = focused ? 'ios-create' : 'ios-create-outline';
                    }

                    return <Icon name={iconName} size={size} color={color} />;
                },
            })}
            tabBarOptions={{
                activeTintColor: Colors.colorPrimary,
                inactiveTintColor: Colors.colorBlack,
            }}>
                
            <Tab.Screen
                name="HomeNavigator"
                component={HomeNavigator}
                options={{ title: "Home" }} />

            <Tab.Screen
                name="CreateNavigator"
                component={CreateNavigator}
                options={{ title: "Create" }} />
        </Tab.Navigator>
    );
};

export default TabNavigator;
import React from 'react';
import { Platform } from "react-native";
import { createStackNavigator } from '@react-navigation/stack';

import CreateScreen from '../screens/CreateScreen';

import Colors from "../constants/Colors";

const Stack = createStackNavigator();

const CreateNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="CreateScreen"
                component={CreateScreen}
                options={{
                    title: 'Create New',
                    headerStyle: {
                        backgroundColor: Platform.OS === "android" ? Colors.colorPrimary : Colors.colorWhite,
                    },
                    headerTintColor: Platform.OS === "android" ? Colors.colorWhite : Colors.colorPrimary,
                }} />
        </Stack.Navigator>
    );
};

export default CreateNavigator;
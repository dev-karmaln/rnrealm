export default Colors = {
    colorPrimary: "#f48b29",
    colorPrimaryDark: "#f48b29",
    colorAccent: "#f48b29",
    colorWhite: "#FFFFFF",
    colorBlack: "#000000",
    colorBackgraound: "#f9f7f7",
    colorRed: "#ff1c1c",
    colorPrimaryDrawerColor: "rgba(82, 37, 70, 0.4)",
};
const UserSchema = {
    name: "User",
    properties: {
        id: "int",
        title: "string",
        description: "string",
        avtarUrl: "string"
    }
}

export default UserSchema;